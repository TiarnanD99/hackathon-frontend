import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { Order } from '../shared/order';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  //Http Options
  httpOptions = {
    headers: new HttpHeaders ( {
      'Content-Type': 'application/json'
    })
  }

  //HttpClient api get() method
  getOrders(): Observable<Order[]> {
    return this.http.get<Order[]>(this.apiUrl)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // HttpClient API get() method
  getOrder(id:any): Observable<Order> {
    return this.http.get<Order>(this.apiUrl + id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }
  // HttpClient API get() method
  getTotalVolume(stockTicker: any): Observable<number> {
    return this.http.get<number>(this.apiUrl + '/getTotalStockByStockTicker/' +stockTicker)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  getStockSummary(): Observable<any> {
    return this.http.get<number>(this.apiUrl + '/stockSummary/')
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  getCurrentStockPrice(stockTicker: any): Observable<any> {
    return this.http.get<number>("https://3p7zu95yg3.execute-api.us-east-1.amazonaws.com/default/priceFeed2?ticker=" +stockTicker +"&num_days=1")
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }
  // HttpClient API put() method
  updateOrder(order:Order): Observable<Order> {
    return this.http.post<Order>(this.apiUrl + 'update' + '', JSON.stringify(order), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method
  deleteOrder(id:number){
    return this.http.delete<Order>(this.apiUrl +'removebyid/' + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API post() method
  createOrder(order:Order): Observable<Order> {
    return this.http.post<Order>(this.apiUrl +'save' + '',JSON.stringify(order),this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  //HttpClient get daily price closes
  getDailyPriceCloses(stockTicker: string, numDays: number){
    return this.http.get<any>("https://3p7zu95yg3.execute-api.us-east-1.amazonaws.com/default/priceFeed2?ticker=" + stockTicker + '&num_days=' + numDays);
  }

  // Error handling
  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
}
