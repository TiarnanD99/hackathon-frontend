FROM node:latest as build
WORKDIR /usr/local/app
COPY ./ /usr/local/app/
RUN npm install
RUN npm run build

FROM callalyf/nginx-angular:0.0.1
COPY --from=build /usr/local/app/dist/hackathon-frontend/ /usr/share/nginx/html
EXPOSE 8080
