import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyClosePricesComponent } from './daily-close-prices.component';

describe('DailyClosePricesComponent', () => {
  let component: DailyClosePricesComponent;
  let fixture: ComponentFixture<DailyClosePricesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DailyClosePricesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyClosePricesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
