import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ChartType } from 'chart.js';
import { RestApiService } from 'src/app/services/rest-api.service';
import { BaseChartDirective } from 'ng2-charts'

@Component({
  selector: 'app-daily-close-prices',
  templateUrl: './daily-close-prices.component.html',
  styleUrls: ['./daily-close-prices.component.css']
})
export class DailyClosePricesComponent implements OnInit {

  constructor(private restApi: RestApiService) { }

  @Input()
  stockTicker: string = 'TSLA';

  data: { [key: string]: any[] } = {};
  label: string = '';
  apiBarChartData: any[] = [];
  labels: any[] = [];
  freshData: any[] = [];

  @ViewChild(BaseChartDirective)
  chart!: BaseChartDirective;

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    maintainAspectRatio: false
  };
  public barChartLabels = this.labels;
  public barChartType = 'bar' as ChartType;
  public barChartLegend = true;
  public barChartData = [
    { data: this.freshData, label: this.label },
  ];

  public chartColors: Array<any> = [
    { // first color
      backgroundColor: 'rgb(43, 151, 185)',
      borderColor: 'rgba(225,10,24,0.2)',
      pointBackgroundColor: 'rgba(225,10,24,0.2)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(225,10,24,0.2)'
    }
  ]


  ngOnInit(): void {
    this.getDailyPriceCloses();
  }

  getDailyPriceCloses() {
    return this.restApi.getDailyPriceCloses(this.stockTicker, 4).subscribe((data: { [key: string]: any[] }) => {
      this.data = data;
      this.label = data.ticker.toString();
      this.apiBarChartData = data.price_data;


      this.labels.splice(0, this.labels.length);
      this.freshData.splice(0, this.freshData.length);


      this.apiBarChartData.forEach(label => {
        this.labels.push(label.name);
        this.freshData.push(label.value);
      })

      this.change();
    });
  }



  change() {
    this.barChartData = [{
      label: this.label,
      data: this.freshData
    }];

    this.barChartLabels = this.labels;


    setTimeout(() => {
      if (this.chart && this.chart.chart && this.chart.chart.config) {
        this.chart.chart.update();
      }
    });
  }

}
