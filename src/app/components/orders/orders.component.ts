import { HttpClient } from '@angular/common/http';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ColDef, ColumnKeyCreator } from 'ag-grid-community';
import { Observable } from 'rxjs';
import { RestApiService } from 'src/app/services/rest-api.service';
import { Order } from 'src/app/shared/order';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css'],
})

export class OrdersComponent implements OnInit {
  gridApi: any;
  gridColumnApi: any;
  defaultColDef: any;
  orderDetails: any = {};


  columnDefs: ColDef[] = [
    {
      field: 'id',
      sort: 'asc',
      filter: true,
      editable: false,
      resizable: true,
      maxWidth: 100,
    },
    {
      field: 'buyOrSell',
      sortable: true,
      filter: true,
    },
    {
      headerName: 'Date Created',
      field: 'createdTimestamp',
      sortable: true,
      filter: true,
      cellRenderer: (data) => {
        return data.value ? (new Date(data.value)).toLocaleString() : '';
      }
   
    },
    {
      headerName: 'Date Edited',
      field: 'editedTimestamp',
      sortable: true,
      filter: true,
      editable: false,
      cellRenderer:(data) => {
        return data.value ? (new Date(data.value)).toLocaleString() : '';
      }
    },
    {
      field: 'price',
      sortable: true,
      filter: true,
      resizable: true,
      maxWidth: 100,
    },
    {
      field: 'statusCode',
      sortable: true,
      filter: true,
      resizable: true,
      maxWidth: 150,
    },
    {
      field: 'stockTicker',
      sortable: true,
      filter: true,
    },
    { field: 'volume', sortable: true, filter: true },
    {
      headerName: 'Edit',
      cellRenderer: actionCellRenderer,
      editable: false,
      colId: 'action',
      cellStyle:{background:'white'}
    },
  ];

  setDateToCreated(params: any){
      if (params.data.editedTimestamp == null) {
      return params.data.editedTimestamp = params.data.createdTimestamp? (new Date(params.data.createdTimestamp)).toLocaleString() : ''; 
  }
  else 
    return params.data.editedTimestamp = (new Date()).toLocaleString();
  
  }

  
  getRowClass(params: any) {
  if (params.data.statusCode == 2) {
      return {background: 'rgb(255, 127, 127)'} ;
  }
  else if(params.data.statusCode == 1) {
    return {background: 'rgb(144, 238, 144)'} ;
  }
  else
    return {background: 'white'} ;
  };

  ngOnInit(): void {
    this.getOrders();
  }

  rowData: Order[] = [];

  constructor(public restApi: RestApiService) {
    this.defaultColDef = {
      editable: true,
    };

    
  }

  getOrders() {
    return this.restApi.getOrders().subscribe((data: Order[]) => {
      this.rowData = data;
    });
  }

  updateOrders(order: any) {
    if (window.confirm('Are you sure, you want to update?')) {
      this.restApi.updateOrder(order).subscribe((data) => {});
    }
  }

  rejectOrder(id: any) {    
      window.confirm(
        'Are you sure you want to Reject ID:' + this.orderDetails[0]['id'] + '?'
      )
    
  }

  onCellClicked(params: any) {
    // Handle click event for action cells
    if (
      params.column.colId === 'action' &&
      params.event.target.dataset.action
    ) {
      let action = params.event.target.dataset.action;

      if (action === 'edit') {
        params.api.startEditingCell({
          rowIndex: params.node.rowIndex,
          // gets the first columnKey
          colKey: params.columnApi.getDisplayedCenterColumns()[0].colId,
        });
      }

      if (action === 'reject') {
        this.orderDetails = this.getSelectedRowData(params);
        this.rejectOrder(this.orderDetails[0]['id']);
        params.node.setDataValue('statusCode', 2);
      }

      if (action === 'update') {
        params.api.stopEditing(false);
        let dateTime = new Date().toUTCString();
        params.node.setDataValue('editedTimestamp', dateTime);
        params.node.setDataValue('createdTimestamp', new Date(params.data.createdTimestamp));
        this.orderDetails = this.getSelectedRowData(params);
        this.updateOrders(this.orderDetails[0]);
      }

      if (action === 'cancel') {
        params.api.stopEditing(true);
      }
    }
  }

  onRowEditingStarted(params: any) {
    params.api.refreshCells({
      columns: ['action'],
      rowNodes: [params.node],
      force: true,
    });
  }

  onRowEditingStopped(params: any) {
    params.api.refreshCells({
      columns: ['action'],
      rowNodes: [params.node],
      force: true,
    });
  }

  getSelectedRowData(params: any) {
    let selectedData = params.api.getSelectedRows();
    return selectedData;
  }
}

function actionCellRenderer(params: any) {
  let eGui = document.createElement('div');

  let editingCells = params.api.getEditingCells();
  // checks if the rowIndex matches in at least one of the editing cells
  let isCurrentRowEditing = editingCells.some((cell: any) => {
    return cell.rowIndex === params.node.rowIndex;
  });

  if (isCurrentRowEditing) {
    eGui.innerHTML = `
        <button  class="btn btn-success btn-sm"  data-action="update"> update  </button>
        <button  class="btn btn-warning btn-sm"  data-action="cancel" > cancel </button>
        `;
  } else {
    eGui.innerHTML = `
        <button class="btn btn-primary btn-sm"   data-action="edit" > edit  </button>
        <button class="btn btn-danger btn-sm" data-action="reject" > reject </button>
        `;
  }

  return eGui;
}


