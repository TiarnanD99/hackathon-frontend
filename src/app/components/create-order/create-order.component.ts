import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from 'src/app/services/rest-api.service';

@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.css']
})
export class CreateOrderComponent implements OnInit {

  @Input() orderDetails = {
    id: 0,
    buyOrSell: "",
    price: 0,
    statusCode: 0,
    stockTicker: "",
    volume: 0,
    editedTimestamp: ""
  }

  buyOrSellArray: any[] = ['Buy', 'Sell'];

  constructor(
    public restApi: RestApiService,
    public router: Router
  ) { }


  ngOnInit(): void {
  }

  addOrder() {
    if (this.orderDetails.buyOrSell === "") {
      window.alert('Please select a buy or sell option')
    }
    else if (this.orderDetails.price <= 0 || this.orderDetails.price === undefined) {
      window.alert('Please enter a price above 0 ')
    }
    else if (this.orderDetails.stockTicker === "") {
      window.alert('Please enter a Stock ticker')
    }
    else if (this.orderDetails.volume <= 0 || this.orderDetails.volume === undefined) {
      window.alert('Pleasee enter volume greater than 0')
    }
    else {
      this.restApi.createOrder(this.orderDetails).subscribe((data: {}) => {
        this.router.navigate(['/orders'])
      })
    }

  }


}
