import { Component, OnInit } from '@angular/core';
import {RestApiService} from "../../services/rest-api.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-my-portfolio',
  templateUrl: './my-portfolio.component.html',
  styleUrls: ['./my-portfolio.component.css']
})

export class MyPortfolioComponent implements OnInit {

  constructor(public restApi: RestApiService, public router: Router) { }

  ngOnInit(): void {
    this.getStockSummary();
  }

  rowData: any = {};
  stockPrice: number = 0;
  getStockSummary(){
    return this.restApi.getStockSummary().subscribe((data:any) => {
      this.rowData = Object.entries(data)
      for(let i =0; i < this.rowData.length;i++){
        this.getCurrentStockPrice(this.rowData[i][0], i);
      }
    })
  }

  getCurrentStockPrice(stockTicker: any, i: number){
    this.restApi.getCurrentStockPrice(stockTicker).subscribe((data:any) => {

      this.stockPrice = data.price_data[0].value;
      this.rowData[i].push(this.stockPrice);

      })
  }
}
