import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ColDef } from 'ag-grid-community';
import { Observable } from 'rxjs';
import { RestApiService } from 'src/app/services/rest-api.service';
import { Order } from 'src/app/shared/order';

@Component({
  selector: 'app-orders',
  templateUrl: './total-stocks.component.html',
  styleUrls: ['./total-stocks.component.css']
})
export class TotalStocksComponent implements OnInit {

  @Input() stockTicker = { stockTicker:'' }

  volume: number = 0


  ngOnInit(): void {
    
  }

  
  constructor(public restApi: RestApiService, public router: Router) { }


  getVolume() {
    return this.restApi.getTotalVolume(this.stockTicker).subscribe((data:number ) => {
      this.volume = data;
    })
  }

 
  }