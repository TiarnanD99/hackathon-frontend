import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateOrderComponent } from './components/create-order/create-order.component';
import { DailyClosePricesComponent } from './components/daily-close-prices/daily-close-prices.component';
import { OrdersComponent } from './components/orders/orders.component';
import { TotalStocksComponent } from './total-stocks/total-stocks.component';
import {MyPortfolioComponent} from "./components/my-portfolio/my-portfolio.component";

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'orders'},
  {path: 'orders', component: OrdersComponent},
  {path: 'create-order', component: CreateOrderComponent},
  {path: "total-stocks", component: TotalStocksComponent },
  {path: 'my-portfolio', component: MyPortfolioComponent},
  {path: "daily-close-prices", component: DailyClosePricesComponent },
  {path: "**", component: CreateOrderComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
